## Real Time Depth First Search Agent

A generic depth first search agent. Developed for demonstration purposes with the game CATCHY.

Tries to periodically (decision pace) find the best action with depth first search on a tree that holds game states.
A node in this tree represents a game state, the branches represent actions from this game state and each layer
of the tree represents a point in time that advances by a defined interval (search pace) with the depth of the tree.
Every layer of the tree in the simulation is alternately assigned a player id to also simulate the opponents.
Furthermore, a simulation depth limit (search depth) is defined.

The decision pace, search pace and search depth should be adjusted to the specific game for reasonable results.

This implementation copies the full game object for each tree node and is therefore NOT SUITED for games that have
LARGE GAME STATES since it runs OUT OF MEMORY!
