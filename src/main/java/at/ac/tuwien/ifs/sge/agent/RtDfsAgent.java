package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeGameAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.core.util.node.RealTimeGameNode;
import at.ac.tuwien.ifs.sge.core.util.tree.DoubleLinkedTree;
import at.ac.tuwien.ifs.sge.core.util.tree.Tree;

import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

/*
    Real Time Depth First Search Agent

    A generic depth first search agent. Developed for demonstration purposes with the game CATCHY.

    Tries to periodically (decision pace) find the best action with depth first search on a tree that holds game states.
    A node in this tree represents a game state, the branches represent actions from this game state and each layer
    of the tree represents a point in time that advances by a defined interval (search pace) with the depth of the tree.
    Every layer of the tree in the simulation is alternately assigned a player id to also simulate the opponents.
    Furthermore, a simulation depth limit (search depth) is defined.

    The decision pace, search pace and search depth should be adjusted to the specific game for reasonable results.

    This implementation copies the full game object for each tree node and is therefore NOT SUITED for games that have
    LARGE GAME STATES since it runs OUT OF MEMORY!
 */
public class RtDfsAgent<G extends RealTimeGame<A, ?>, A> extends AbstractRealTimeGameAgent<G, A> {

    /*
        The agent is started as a new process that gets passed three arguments:
            1. the player id of the player it is going to represent
            2. the player name of the player it is going to represent
            3. the game class name of the game it should play. This is only relevant if the agent supports multiple games.

        Since the 'Main-Class' attribute of the JAR file defined in the build.gradle points to this class
        a main method is defined which gets called when the engine starts the agent. The agent is then instantiated
        and started through the start() method, which also starts the agent's communication with the engine server.
    */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends RealTimeGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new RtDfsAgent<>(gameClass, playerId, playerName, 0);
        agent.start();
    }

    private static final int DEFAULT_SEARCH_PACE_MS = 1010;
    private static final int DEFAULT_SEARCH_DEPTH = 5;
    private static final int DEFAULT_DECISION_PACE_MS = 1050;

    private final int searchPaceMs;
    private final int searchDepth;
    private final int decisionPaceMs;

    private Future<?> dfsIterationFuture;
    private ScheduledExecutorService scheduledExecutorService;

    // Constructor with default parameters
    public RtDfsAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        this(DEFAULT_SEARCH_PACE_MS,
                DEFAULT_SEARCH_DEPTH,
                DEFAULT_DECISION_PACE_MS,
                gameClass, playerId, playerName, logLevel);
    }

    /*
        A constructor calling the super constructor is required when extending the abstract agent.

        Since this agent can play any real time game the game class has a generic type.
     */
    public RtDfsAgent(int searchPaceMs,
                       int searchDepth,
                       int decisionPaceMs,
                       Class<G> gameClass, int playerId, String playerName, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
        this.searchPaceMs = searchPaceMs;
        this.searchDepth = searchDepth;
        this.decisionPaceMs = decisionPaceMs;
    }

    /*
        The underlying structure of the abstract agent uses an ExecutorService to handle all its threads.
        The getMinimumNumberOfThreads() can be overridden to increases the number of threads the agent needs,
        but should always call the super method to ensure that the abstract agents threads are considered.

        This agent needs one additional thread.
     */
    @Override
    protected int getMinimumNumberOfThreads() {
        return super.getMinimumNumberOfThreads() + 1;
    }

    /*
        When the abstract agent receives an update from the server it is applied to the agents game after which
        the method onGameUpdate(A action, ActionResult result) is called. The parameters represent the
        action that has been applied as well as the result it created. The updated game state can be found
        in the protected field 'game' of the abstract agent implementation.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent does not react to game updates.
     */
    @Override
    protected void onGameUpdate(HashMap<A, ActionResult> actionsWithResult) {}

    /*
        In case of an action that was sent to the server and is not valid or no longer valid, the server responds
        with an InvalidActionEvent which contains the action that has been rejected. The abstract agent then
        calls the onActionRejected(A action) method.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent does not react to rejected actions.
     */
    @Override
    protected void onActionRejected(A action) {}

    /*
        After every agent is setup, the engine server issues a StartGameEvent which indicates that it is now
        ready to receive and process actions from the agents.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The agent starts a new thread in which the computation happens.
     */
    @Override
    public void startPlaying() {
        dfsIterationFuture = pool.submit(this::play);
    }

    /*
        After the game is over the agent receives a TearDownEvent event, which signals it to stop
        the computation and shut down gracefully.

        The thread that runs the depth first search gets interrupted.
    */
    @Override
    public void shutdown() {
        dfsIterationFuture.cancel(true);
    }

    private void play() {
        A lastDeterminedAction = null;

        // Indefinitely try to find the best action of the current game state at a fixed decision pace
        for (;;) {
            try {

                // Copy the game state and apply the last determined action, since this action was not yet accepted and sent back
                // from the engine server at this point in time
                var gameState = copyGame();
                if (lastDeterminedAction != null) {
                    gameState.scheduleActionEvent(new GameActionEvent<>(playerId, lastDeterminedAction, gameState.getGameClock().getGameTimeMs() + 1));
                }
                // Advance the game state in time by the decision pace since this is the point in time that the next best action will be sent
                gameState.advance(decisionPaceMs);

                // Create a new tree with the game state as root
                var dfsTree = new DoubleLinkedTree<>(new RealTimeGameNode<>(playerId, gameState, null));
                // Tell the garbage collector to try recycling/reclaiming unused objects
                System.gc();

                // Calculate the time of the next decision
                var now = System.currentTimeMillis();
                var timeOfNextDecision = now + decisionPaceMs;

                log._info_();
                log.info("now = " + now);
                log.info("timeOfNextDecision = " + timeOfNextDecision);

                // Find the next action before the time of next decision is reached
                var action = findBestAction(dfsTree, timeOfNextDecision);

                // If the whole tree has been explored before reaching the time of next decision: wait
                var remainingTime = timeOfNextDecision - System.currentTimeMillis();
                if (remainingTime > 0)
                    Thread.sleep(remainingTime);

                // Send the best action to the engine server.
                if (dfsTree.isLeaf()) {
                    log.info("Could not find a move! Doing nothing...");
                } else {
                    log._info_();
                    log.info("Determined next action: " + action);
                    if (action != null) {
                        sendAction(action, System.currentTimeMillis() + 10);
                    }
                }
                lastDeterminedAction = action;

            } catch (Exception e) {
                if (!(e instanceof InterruptedException))
                    log.printStackTrace(e);
                break;
            }
        }
    }


    /*
        Performs depth first search on the game tree finding the heuristically optimal game state
        as long as the decision pace and search depth allow it.
        Returns the top level action that is responsible for the branch that leads to the best
        game state.
    */
    private A findBestAction(Tree<RealTimeGameNode<A>> tree, long timeOfNextDecision) {

        var node = tree.getNode();
        var depth = 0;

        var bestGame = node.getGame();

        A bestAction = null;
        int stepsToBestAction = searchDepth;
        A currentTopLevelAction = null;

        var iterations = 0;

        while((!tree.isRoot() || node.hasUnexploredActions()) && System.currentTimeMillis() < timeOfNextDecision) {


            A action = null;
            if (!tree.isLeaf()) {
                if (node.hasUnexploredActions()) {
                    action = node.popUnexploredAction();
                } else {
                    depth--;
                    tree = tree.getParent();
                    node = tree.getNode();
                    continue;
                }
            }

            if (tree.isRoot())
                currentTopLevelAction = action;

            iterations++;
            depth++;

            // Copy the game state and advance it by the search pace
            var game = node.getGame();
            var nextGameState = game.copy();
            if (action != null)
                nextGameState.scheduleActionEvent(new GameActionEvent<>(node.getPlayerId(), action, game.getGameClock().getGameTimeMs() + 1));

            // If we have partial information (Fog of War) the result of some actions might be ambiguous leading in an ActionException
            try {
                nextGameState.advance(searchPaceMs);

                var gameComparison = gameComparator.compare(nextGameState, bestGame);
                if ((gameComparison > 0 && stepsToBestAction >= depth) || (gameComparison >= 0 && stepsToBestAction > depth)) {
                    stepsToBestAction = depth;
                    bestGame = nextGameState;
                    bestAction = currentTopLevelAction;
                }
            } catch (ActionException e) {
                log.debug("simulation reached invalid game state (partial information). ignoring...");
            }

            // Expand the tree by the new game state
            var expandedTree = new DoubleLinkedTree<>(new RealTimeGameNode<>(node.getNextPlayerId(), nextGameState, action));
            tree.add(expandedTree);

            // Go back in depth if the search depth is reached, otherwise apply depth first
            if (depth == searchDepth) {
                depth--;
            } else {
                tree = expandedTree;
                node = tree.getNode();
            }
        }

        log.debug("iterations: " + iterations);

        return bestAction;
    }
}
